# File Storage Microservice
  This repo contain microservice about file storage. By it has feature such as store file, get file, update file and delete file

## High Level Design
![Alt text](https://gitlab.com/file-storage-wibwub/service-compose/-/raw/main/hi_design.png "Title")

## Feature
- [x] CRUD
- [ ] Authentication
- [ ] Authorize
- [ ] Caching
  
## Prerequisite
 - install docker with docker-compose
 -  see [postman collection](https://www.postman.com/WaveBlur/workspace/s-public-workspace/collection/4296154-48073b88-386d-43df-b289-5f7f149ef7b1?action=share&creator=4296154&active-environment=4296154-1bb8cb81-474a-4703-9692-574c84e710d8)


## Step deploy on local docker desktop
- clone this repo by command `git clone --recurse-submodules git@gitlab.com:file-storage-wibwub/service-compose.git`
- run `docker-compose up -d`


